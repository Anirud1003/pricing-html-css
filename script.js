function result() {
    let monthlyPrices = ["19.99", "24.99", "39.99"]
    let yearlyPrices = ["199.99", "249.99", "399.99"]
    let prices = document.getElementsByClassName("price")
    console.log(prices)
    if (prices[0].innerHTML === monthlyPrices[0]) {
        Object.keys(prices).map((key)=>
            prices[key].innerHTML = yearlyPrices[key]
        )
    } else {
        Object.keys(prices).map((key)=>
            prices[key].innerHTML = monthlyPrices[key]
        )
    }
}
